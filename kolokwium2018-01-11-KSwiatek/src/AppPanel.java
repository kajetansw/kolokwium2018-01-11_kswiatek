import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Random;

import javax.swing.JPanel;

public class AppPanel extends JPanel {

	Point pointStart = null;
    Point pointEnd = null;
    Random rand = new Random();
    Color clr;
    int it = 0;
     
	public AppPanel() {
		this.setVisible(true);
		this.setBounds(0, 0, 500, 500);
		this.setLayout(null);
		this.setBackground(Color.lightGray);
		
		this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
            	clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
                pointStart = e.getPoint();
            }

            public void mouseReleased(MouseEvent e) {
                //pointStart = null;
            	it++;
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                pointEnd = e.getPoint();
            }

            public void mouseDragged(MouseEvent e) {
                pointEnd = e.getPoint();
                repaint();
            }
        });
	}

     public void paint(Graphics g) {
         super.paint(g);
         if (pointStart != null) {
             g.setColor(clr);
             g.drawRect(pointStart.x, pointStart.y, pointEnd.x-pointStart.x, pointEnd.y-pointStart.y);
         }
     }
}
