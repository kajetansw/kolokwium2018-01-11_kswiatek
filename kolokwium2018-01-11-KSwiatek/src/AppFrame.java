import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class AppFrame extends JFrame {
	
	final AppPanel mainPanel = new AppPanel();
	
	public AppFrame() {
		super("Kolowkium");
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setBounds(200, 100, 500, 500);
        setResizable(false);
        
        this.getContentPane().add(mainPanel);
        
	}
}
